package com.zrv.adressbook;

import java.util.Comparator;

public class SortedByLastName implements Comparator<Contacts> {

    public int compare(Contacts lname1, Contacts lname2) {

        String str1 = lname1.getLname();
        String str2 = lname2.getLname();

        return str1.compareTo(str2);
    }
}