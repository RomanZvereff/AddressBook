package com.zrv.adressbook;

public class Contacts {

    private String fname;
    private String lname;
    private String phone;

    Contacts(String f, String l, String p) {
        fname = f;
        lname = l;
        phone = p;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public String getPhone() {
        return phone;
    }
}