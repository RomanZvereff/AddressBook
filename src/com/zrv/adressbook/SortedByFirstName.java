package com.zrv.adressbook;

import java.util.Comparator;

public class SortedByFirstName implements Comparator<Contacts> {

    public int compare(Contacts fname1, Contacts fname2) {

        String str1 = fname1.getFname();
        String str2 = fname2.getFname();

        return str1.compareTo(str2);
    }
}