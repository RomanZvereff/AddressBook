package com.zrv.adressbook;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class AddressBook {

    int userAnswer = 0;
    int answer = 9;

    public void addressBook() {

        final String[] MENU = {
                "What do you want to do?",
                "To sort by First name write - 1",
                "To sort by Last name write - 2",
                "To filter contacts by First & Last names write - 3",
                "To filter contacts by Phone number write - 4",
                "Write \"0\" to EXIT"
        };

        Scanner scanner = new Scanner(System.in);

        List<Contacts> contacts = new ArrayList<>();

        contacts.add(new Contacts("Steven", "Jobs", "555-789-123"));
        contacts.add(new Contacts("Bill", "Gates", "555-014-036"));
        contacts.add(new Contacts("Bill", "Smith", "555-000-987"));
        contacts.add(new Contacts("Ann", "Miller", "555-147-696"));
        contacts.add(new Contacts("Sara", "Miller", "555-789-111"));
        contacts.add(new Contacts("John", "Phillips", "555-556-031"));
        contacts.add(new Contacts("Donald", "Trump", "555-789-789"));
        contacts.add(new Contacts("Max", "Paine", "555-123-987"));
        contacts.add(new Contacts("Emma", "Jobs", "555-025-852"));

        while (userAnswer != answer) {
            System.out.println("===Contacts list:");
            for (Contacts i : contacts) {
                System.out.println(i.getFname() + " " + i.getLname() + " " + i.getPhone());
            }
            System.out.println("===============================");
            drawMenu(MENU);
            System.out.println("=> Please write you answer => ");
            answer = scanner.nextInt();

            if (answer == 1) {
                Collections.sort(contacts, new SortedByFirstName());
                System.out.println("===Sorted contacts by First name:");
                for (Contacts i : contacts) {
                    System.out.println(i.getFname() + " " + i.getLname() + " " + i.getPhone());
                }
                System.out.println();
            }

            if (answer == 2) {
                Collections.sort(contacts, new SortedByLastName());
                System.out.println("===Sorted contacts by Last name:");
                for (Contacts i : contacts) {
                    System.out.println(i.getFname() + " " + i.getLname() + " " + i.getPhone());
                }
                System.out.println();
            }

            if (answer == 3) {
                System.out.println("=> Please write the desired First name => ");
                String first = scanner.nextLine();
                System.out.println("=> Please write the desired Last name => ");
                String last = scanner.nextLine();

                System.out.println("===Filtered contacts by First & Last names:");
                List<String> filterByNameAndSername = contacts.stream()
                        .filter(fname -> fname.getFname().contains(first)) //&& lname -> lname.getLname().contains(last))
                        .filter(lname -> lname.getLname().contains(last))
                        .map(contacts1 -> contacts1.getFname() + " " + contacts1.getLname() + " " + contacts1.getPhone())
                        .collect(Collectors.toList());
                System.out.println(filterByNameAndSername);
                System.out.println();
            }

            if (answer == 4) {
                System.out.println("=> Please write the desired phone number => ");
                String phone1 = scanner.nextLine();

                System.out.println("===Filtered contacts by Phone number:");
                List<String> filterByPhoneNumber = contacts.stream()
                        .filter(phone -> phone.getPhone().contains(phone1))
                        .map(contacts1 -> contacts1.getFname() + " " + contacts1.getLname() + " " + contacts1.getPhone())
                        .collect(Collectors.toList());
                System.out.println(filterByPhoneNumber);
                System.out.println();
            }
        }
        System.out.println("EXIT!");
    }

    public static void drawMenu(String[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }
}